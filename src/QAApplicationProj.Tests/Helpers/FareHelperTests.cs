﻿using QAApplicationProj.Helpers;
using Xunit;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        // Test cases testing both values being positive and/or zero.
        [Fact]
        public void AddFares_ReturnsPass_BothPositive() {
            decimal firstFare = 3;
            decimal secondFare = 8;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        [Fact]
        public void AddFares_ReturnsPass_FirstZero()
        {
            decimal firstFare = 0;
            decimal secondFare = 33;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        [Fact]
        public void AddFares_ReturnsPass_SecondZero()
        {
            decimal firstFare = 56;
            decimal secondFare = 0;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        //Test cases testing one value being positive and one negative.
        [Fact]
        public void AddFares_ReturnsFail_FirstNegative()
        {
            decimal firstFare = -20;
            decimal secondFare = 40;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        [Fact]
        public void AddFares_ReturnsFail_SecondNegative()
        {
            decimal firstFare = 87;
            decimal secondFare = -87;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }  

        /*Additional test cases, testing combinations of one value being
        zero and the other being negative*/
        [Fact]
        public void AddFares_ReturnsFail_FirstZeroSecondNegative()
        {
            decimal firstFare = 0;
            decimal secondFare = (decimal)-0.1;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        [Fact]
        public void AddFares_ReturnsFail_FirstNegativeSecondZero()
        {
            decimal firstFare = -100000000000000;
            decimal secondFare = 0;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        [Fact]
        public void AddFares_ReturnsPass_BothZero()
        {
            decimal firstFare = 0;
            decimal secondFare = 0;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }

        //Decimal overflow test case
        [Fact]
        public void AddFares_ReturnsFail_DecimalOverflow()
        {
            decimal firstFare = decimal.MaxValue;
            decimal secondFare = 1;

            var fareTest = FareHelper.AddFares(firstFare, secondFare);
            Assert.True(true);
        }
    }
}
