﻿using QAApplicationProj.Controllers;
using QAApplicationProj.Data.Repositories;
using QAApplicationProj.Data.Repositories.Interfaces;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;
using Xunit;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests {
        private readonly CarController _controller;
        public CarControllerTests() {
            /*CarRepository contains a list of cars that can be used to test
            rather than connecting to an external data source.  The new instance 
            of CarRepoitory can be passed to the controller to test new/existing 
            car functionality*/
            ICarRepository carRepository = new CarRepository();
            _controller = new CarController(carRepository);            
        }

        // Test case testing successful new car creation.
        [Fact]
        public void CreateCar_ReturnsPass_NewCar() {
            CarRequest carReq = new CarRequest();
            carReq.Make = "Honda";
            carReq.Model = "Accord";
            carReq.Year = 2017;

            CreateCarResponse create =  _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        //Test case where car attempting to be added already exists.
        [Fact]
        public void CreateCar_ReturnsFail_ExistingCar()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = "Ferrari";
            carReq.Model = "Testarossa";
            carReq.Year = 1989;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        //Testing invalid requests where some or all fields are null or zero (year).
        [Fact]
        public void CreateCar_ReturnsFail_InvalidRequest_NullMake()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = "";
            carReq.Model = "Sentra";
            carReq.Year = 2014;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        [Fact]
        public void CreateCar_ReturnsFail_InvalidRequest_NullModel()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = "Audi";
            carReq.Model = "";
            carReq.Year = 2013;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        [Fact]
        public void CreateCar_ReturnsFail_InvalidRequest_InvalidYear()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = "Toyota";
            carReq.Model = "Tundra";
            carReq.Year = 0;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        [Fact]
        public void CreateCar_ReturnsFail_InvalidRequest_NullMakeModel()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = "";
            carReq.Model = "";
            carReq.Year = 2009;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }

        [Fact]
        public void CreateCar_ReturnsFail_InvalidRequest_InvalidAll()
        {
            CarRequest carReq = new CarRequest();
            carReq.Make = null;
            carReq.Model = null;
            carReq.Year = 0;

            CreateCarResponse create = _controller.CreateCar(carReq);

            //if create was successful, test case passes
            Assert.True(create.WasSuccessful);
        }
    }
}
